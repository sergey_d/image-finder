import BASE from '../constants/base';

const SCROLL_OFFSET = BASE.HEADER_SIZE + BASE.BASE_MARGIN;

const scrollGallery = (page) => {
  const galleries = document.querySelectorAll('.gallery-list');
  const galleryIndex = page - 1;

  const activeGallery = galleries[galleryIndex];

  const galleryRect = activeGallery.getBoundingClientRect();
  const bodyRect = document.body.getBoundingClientRect();
  const offset = galleryRect.top - bodyRect.top;

  window.scrollTo({
    top: offset - SCROLL_OFFSET,
    left: 0,
    behavior: 'smooth',
  });

};

export default scrollGallery;
