import notification from './notification';
import * as basicLightbox from 'basiclightbox';
import 'basiclightbox/dist/basicLightbox.min.css';

const showError = (message) => {
  notification.errorMessage(message);
};

const openLightBox = (imageUrl) => {
  console.log('openLightBox');
  basicLightbox.create(`
  <img src='${imageUrl}' alt=''>
`).show();
};

const utils = {
  showError,
  openLightBox,
};

export default utils;
