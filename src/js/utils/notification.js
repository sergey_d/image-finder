import '@pnotify/core/dist/PNotify.css';
import '@pnotify/core/dist/BrightTheme.css';
import { info, error } from '@pnotify/core';

const notification = {
  errorMessage: (text) => {
    error({
      text,
    });
  },

  infoMessage: (text) => {
    info({
      text,
    });
  },
};

export default notification;
