import API from '../constants/api';
import IMAGES from '../constants/images';

const apiService = {
  getImages(searchQuery, page) {

    const { ORIENTATION, PER_PAGE, TYPE } = IMAGES;

    const URL = `${API.BASE_URL}?image_type=${TYPE}&orientation=${ORIENTATION}&q=${searchQuery}&page=${page}&per_page=${PER_PAGE}&key=${API.API_KEY}`;

    return fetch(URL).then(response => {

      if (!response.ok) {
        throw response;
      }

      return response.json();
    });
  },
};

export default apiService;
