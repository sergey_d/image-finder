const IMAGES = {
  PER_PAGE: 12,
  ORIENTATION: 'horizontal',
  TYPE: 'photo'
};

export default IMAGES;
