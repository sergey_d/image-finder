const BASE = {
  HEADER_SIZE: 48,
  BASE_MARGIN: 12,
};

export default BASE;
