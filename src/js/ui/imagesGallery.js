import MarkupGenerator from './markupGenerator';
import utils from '../utils/utils';

class ImagesGallery {

  // imageSelector - id for lightbox plugin
  constructor(selector, template, lightboxAttribute) {
    this.page = 1;
    this.totalPages = 0;
    this.markup = new MarkupGenerator(selector, template);
    if (lightboxAttribute) {
      this.initLightBox(selector, lightboxAttribute);
    }
  }

  incrementPage() {
    this.page += 1;
  }

  resetPage() {
    this.page = 1;
  }

  reset() {
    this.resetPage();
    this.markup.destroy();
  }

  init(data) {
    this.markup.createMarkup(data);
  }

  update(data) {
    this.markup.updateMarkup(data);
  }

  setTotalPages(number) {
    this.totalPages = number;
  }

  initLightBox(wrapper, lightboxAttribute) {
    wrapper.addEventListener('click', (event) => {
      const target = event.target;
      const url = target.dataset[lightboxAttribute];
      if (url) {
        console.log(target);
        utils.openLightBox(url);
      }
    });
  }

}

export default ImagesGallery;
