import MarkupGenerator from './markupGenerator';

const FORM_ID = '#search-form';

class SearchForm {
  constructor(selector, template) {
    this.markup = new MarkupGenerator(selector, template);
    this.form = null;
    this.formInput = null;
  }

  init(data) {
    this.markup.createMarkup(data);
    this.form = document.querySelector(FORM_ID);
    this.formInput = document.querySelector(`${FORM_ID} input`);
  }

  getQuery() {
    return this.formInput ? this.formInput.value : null;
  }

  onSubmit(callback) {
    this.form.addEventListener('submit', callback);
  }

}

export default SearchForm;
