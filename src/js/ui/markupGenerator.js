class MarkupGenerator {

  constructor(selector, template) {
    this.selector = selector;
    this.template = template;
  }

  createMarkup(data) {
    this.destroy();
    this.renderMarkup(data);
  }

  updateMarkup(data) {
    this.renderMarkup(data);
  }

  renderMarkup(string) {
    const markupString = this.template(string);
    this.selector.insertAdjacentHTML('beforeend', markupString);
  }

  destroy() {
    this.selector.innerHTML = '';
  }
}

export default MarkupGenerator;
