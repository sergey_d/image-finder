import './styles.scss';

import SearchForm from './js/ui/searchForm';
import ImagesGallery from './js/ui/imagesGallery';

import galleryTemplate from './tamplates/gallery.hbs';
import searchFormTemplate from './tamplates/searchForm.hbs';

import apiService from './js/api/apiService';
import utils from './js/utils/utils';
import IMAGES from './js/constants/images';
import scrollGallery from './js/utils/scroll';

const refs = {
  formWrapper: document.querySelector('.form-wrapper'),
  galleryBox: document.querySelector('.gallery-box'),
  loadMoreBtn: document.querySelector('.load-btn'),
};

// Экземпляр формы
const searchForm = new SearchForm(refs.formWrapper, searchFormTemplate);
searchForm.init();

// Экземпляр галереи
const lightboxAttribute = 'largeimageurl';

const imagesGallery = new ImagesGallery(refs.galleryBox, galleryTemplate, lightboxAttribute);

const searchImages = (event) => {
  event.preventDefault();
  const query = searchForm.getQuery();

  if (!query) {
    utils.showError('Invalid query!');
    imagesGallery.reset();
    return;
  }

  imagesGallery.reset();
  getImages(query, imagesGallery.page);
};

const loadMore = () => {
  imagesGallery.incrementPage();
  const query = searchForm.getQuery();
  updateImages(query, imagesGallery.page);
};

// получем новые изображение
const getImages = (query, page) => {

  apiService.getImages(query, page)
    .then(response => {
      setTotalPages(response.total);
      const data = response.hits;

      if (data && data.length) {
        imagesGallery.init(data);
        initLoadMore();
      } else {
        utils.showError('Not found!');
      }
    })
    .catch(err => console.log(err));
};

// добавляем изображения
const updateImages = (query, page) => {

  apiService.getImages(query, page)
    .then(response => {
      setTotalPages(response.total);
      const data = response.hits;

      if (data && data.length) {
        imagesGallery.update(data);
        scrollGallery(page);
      } else {
        utils.showError('Not found!');
      }
    })
    .catch(err => console.log(err));
};

// вызываем метод из класса и передаем коллбэк
searchForm.onSubmit(searchImages);

const setTotalPages = (totalItems) => {
  const total = Math.ceil(totalItems / IMAGES.PER_PAGE);
  imagesGallery.setTotalPages(total);
};

const initLoadMore = () => {
  window.addEventListener('scroll', (e) => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      loadMore();
    }
  });
};
